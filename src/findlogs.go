package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path"
	"strconv"
	"strings"
	"time"
)

const DAYS_FROM_SECONDS = 60 * 60 * 24

var now time.Time

func init() {
	tmp := time.Now()
	now = time.Date(tmp.Year(), tmp.Month(), tmp.Day(), 0, 0, 0, 0, time.UTC)
}

func main() {

	if len(os.Args) != 3 {
		exec_name := execName(os.Args[0])
		fmt.Println("usage:", exec_name, "<days> <path>")
		os.Exit(0)
	}

	var days int = days(os.Args[1])
	var root string = os.Args[2]
	scan(days, root)

}

func scan(days int, root string) {

	files, err := ioutil.ReadDir(root)
	if err != nil {
		log.Fatal(err)
	}

	for index := range files {
		var fi os.FileInfo = files[index]

		if fi.IsDir() {
			var fname = path.Join(root, fi.Name())
			if ft, err := createTime(fi); err {
				diff := now.Unix() - ft.Unix()
				if allow(diff, days) {
					fmt.Println(fname)
				}
			}
			scan(days, fname)
		}
	}

}

func allow(diff int64, days int) bool {
	var other int = int(diff / DAYS_FROM_SECONDS)
	return other > days
}

func createTime(fi os.FileInfo) (time.Time, bool) {

	name := fi.Name()
	if len(name) != 10 {
		return time.Now(), false
	}

	year := convert(name[0:4], 1000, 3000)
	month := convert(name[5:7], 1, 12)
	day := convert(name[8:10], 1, 31)

	if day == -1 || month == -1 || year == -1 {
		return time.Now(), false
	}

	return time.Date(year, time.Month(month), day, 0, 0, 0, 0, time.UTC), true

}

func convert(value string, min int, max int) int {
	val, err := strconv.Atoi(value)
	if err != nil {
		return -1
	}
	if val < min || val > max {
		return -1
	}
	return val
}

func days(value string) int {
	if res, err := strconv.Atoi(value); err == nil {
		return res
	}
	return 10
}

func execName(value string) string {
	index := strings.LastIndex(value, "/")
	return value[index+1:]
}
